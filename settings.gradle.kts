pluginManagement {
    val kotlinVersion:String by settings
    val jfrogVersion:String by settings
    val dokkaVersion:String by settings
    val version:String by settings
    repositories {
        mavenLocal()
        maven {
            setUrl("https://dl.bintray.com/skerna/maven")
        }
        google()
        gradlePluginPortal()
        mavenCentral()
        jcenter()
    }
    plugins {
        id("org.jetbrains.kotlin.jvm").version(kotlinVersion)
        id("com.jfrog.bintray").version(jfrogVersion)
        id("org.jetbrains.dokka").version(dokkaVersion)
    }
}