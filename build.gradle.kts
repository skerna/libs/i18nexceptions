plugins {
    id("org.jetbrains.kotlin.jvm")
    id("io.skerna.libs.gradle.base") version "1.0.0"
}

packageSpec{
    metadata{
        organization("skerna")
        description("This library provide support for JVM Exceptions multilang")
        versionControlRepository("https://gitlab.com/skerna/libs/i18nexceptions")
        licences{
            licence("MIT")
        }
        developers{
            developer("Ronald Cardenas")
        }
        withLabels("library","jvm")
    }
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.6.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.6.2")
}

tasks.withType<Test> {
    useJUnitPlatform()
}