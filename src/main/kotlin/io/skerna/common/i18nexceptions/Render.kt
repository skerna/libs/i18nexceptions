package io.skerna.common.i18nexceptions


import java.util.Locale
import java.util.Optional

interface Render {
    /**
     * Renderiza una expceion i18n
     * @param i18NExceptionBase
     * @param locale
     * @return
     */
    fun render(i18NExceptionBase: I18NException, locale: Locale): Optional<RenderResult>

    /**
     * Renderiza una exception sin especificar el idioma
     * @param i18NExceptionBase
     * @return
     */
    fun render(i18NExceptionBase: I18NException): Optional<RenderResult>

    companion object {


        /**
         * Crea un render
         * @param catalog
         * @return
         */
        fun create(catalog: MessageCatalog): Render {
            return ExceptionRender(catalog)
        }
    }

}
