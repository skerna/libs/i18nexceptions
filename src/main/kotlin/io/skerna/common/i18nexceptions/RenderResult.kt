package io.skerna.common.i18nexceptions

import java.util.Objects

@SuppressWarnings
open class RenderResult(val errorCode: String, val mensaje: String, val additionalMessages:Map<String,String>): JsonEncoded {

    override fun toString(): String {
        return "RenderResult{" +
                "codigo='" + errorCode + '\''.toString() +
                ", mensaje='" + mensaje + '\''.toString() +
                ", additionalMessages='"+(additionalMessages?:"") + '\''.toString() +
                '}'.toString()
    }

    override fun encodeAsJson(): String {
        return """
            {
                "codigo":"$errorCode",
                "message":"$mensaje",
                "detailes":"$additionalMessages"
            }
        """.trimIndent()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as RenderResult?
        return errorCode == that!!.errorCode && mensaje == that.mensaje
    }

    override fun hashCode(): Int {
        return Objects.hash(errorCode, mensaje)
    }
}
