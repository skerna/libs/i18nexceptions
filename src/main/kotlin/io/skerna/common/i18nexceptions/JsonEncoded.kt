package io.skerna.common.i18nexceptions

interface JsonEncoded {
    fun encodeAsJson():String
}