package io.skerna.common.i18nexceptions

interface I18NException: Renderable {
    /**
     * Retorna el código a renderizar
     * @return
     */
    fun getErrorCode(): String

    /**
     * Retorna una lista de codigos adicionales
     * @return List<String>
     */
    fun getAdditionalErrorCodes():List<String>


    /**
     * Check if current exception has addiotional Exceptions error
     * codes
     * @return Boolean
     */
    fun hasAdditionalErrorCodes():Boolean

    /**
     * Retorna el render
     * para la interfaz cliente
     * @return
     */
    fun getRender(): Render


}
