package io.skerna.common.i18nexceptions

object Utils {
    /**
     * Determina si una cadena de caracteres es nula o vacia.
     * @param text
     * @return
     */
    fun isNullOrEmpty(text: String?): Boolean {
        return text.isNullOrBlank()
    }

}
