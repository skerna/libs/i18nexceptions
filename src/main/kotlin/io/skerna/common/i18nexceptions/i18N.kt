package io.skerna.common.i18nexceptions

import io.skerna.common.i18nexceptions.Utils.isNullOrEmpty
import java.util.ResourceBundle


@SuppressWarnings
internal object i18N {
    val BUNDLE_INTERNAL_NAME = "i18nexceptions"

    val CODE = dataInitializer(I18nKeys.CODE_KEY)
    val MESSAGE = dataInitializer(I18nKeys.MESSAGE_KEY)
    val DETAILS = dataInitializer(I18nKeys.DETAILS_KEY)
    val SPECIFIC = dataInitializer(I18nKeys.SPECIFIC_KEY)

    internal enum class I18nKeys private constructor(internal val key: String, internal val defaultValue: String) {
        CODE_KEY("CODE", "code"),
        MESSAGE_KEY("MESSAGE", "message"),
        DETAILS_KEY("DETAILS", "details"),
        SPECIFIC_KEY("SPECIFIC", "specific")

    }


    private fun dataInitializer(wichKey: I18nKeys): String {
        var bunlde: ResourceBundle? = null
        try {
            bunlde = ResourceBundle.getBundle(BUNDLE_INTERNAL_NAME)
        } catch (e: Exception) {
        }

        var name = when (wichKey) {
            I18nKeys.CODE_KEY -> {
                val _cod = if (bunlde != null) bunlde.getString(I18nKeys.CODE_KEY.key) else I18nKeys.CODE_KEY.defaultValue
                return if (isNullOrEmpty(_cod)) "Code" else _cod
            }
            I18nKeys.DETAILS_KEY -> {
                val _det = if (bunlde != null) bunlde.getString(I18nKeys.DETAILS_KEY.key) else I18nKeys.DETAILS_KEY.defaultValue
                return if (isNullOrEmpty(_det)) "Details" else _det
            }
            I18nKeys.MESSAGE_KEY -> {
                val _mess = if (bunlde != null) bunlde.getString(I18nKeys.MESSAGE_KEY.key) else I18nKeys.MESSAGE_KEY.defaultValue
                return if (isNullOrEmpty(_mess)) "Message" else _mess
            }
            I18nKeys.SPECIFIC_KEY -> {
                val _spec = if (bunlde != null) bunlde.getString(I18nKeys.SPECIFIC_KEY.key) else I18nKeys.SPECIFIC_KEY.defaultValue
                return if (isNullOrEmpty(_spec)) "Spec" else _spec
            }
            else ->  "Unknown"
        }
        return name
    }


}
