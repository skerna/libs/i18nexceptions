package io.skerna.common.i18nexceptions


import java.lang.RuntimeException

/**
 * Created by ronald on 02/02/18.
 */
abstract class StandardRuntimeException  : RuntimeException, I18NException {
    private val errorCode:String
    private val additionalCodes: MutableList<String> by lazy { mutableListOf<String>() }
    private val additionalMessages: MutableList<String> by lazy { mutableListOf<String>() }
    private val _render: Render by lazy { initialize_render() }


    constructor(errorCode: String):super(""){
        this.errorCode = errorCode
    }

    constructor( errorCode: String,message: String?) : super(message) {
        this.errorCode = errorCode
    }

    constructor( errorCode: String,message: String?, cause: Throwable?) : super(message, cause) {
        this.errorCode = errorCode
    }

    constructor( errorCode: String,cause: Throwable?) : super(cause) {
        this.errorCode = errorCode
    }

    constructor( errorCode: String,message: String?, cause: Throwable?, enableSuppression: Boolean, writableStackTrace: Boolean) : super(message, cause, enableSuppression, writableStackTrace) {
        this.errorCode = errorCode
    }


    open fun appendMessage(message: String) = apply {
        additionalMessages.add(message)
    }
    open fun appendErrorCode(errorCode: String) = apply{
        additionalCodes.add(errorCode)
    }

    override fun getErrorCode(): String  = errorCode

    override fun getAdditionalErrorCodes(): List<String>  = additionalCodes

    override fun hasAdditionalErrorCodes(): Boolean = this::additionalCodes.isLazyInitialized

    /**
    @Synchronized
    override fun fillInStackTrace(): Throwable {
        return this
    } */

    override fun toString(): String {

        return "\n[" + i18N.CODE + "]=" + errorCode +
                "\n[" + i18N.MESSAGE + "]=" + message +
                "\n[" + i18N.DETAILS + "]=" + additionalMessages.toString()
    }


    override val message: String?
        get() {
            var message = ""
            val result = _render.render(this)
            if (result.isPresent) {
                val _render = result.get()
                message = _render.mensaje
            }
            return message
        }

    override fun render(): RenderResult {
        val renderResult = _render.render(this)
        if(renderResult.isEmpty){
            this.printStackTrace();
        }
        return renderResult.get()
    }

    private fun initialize_render(): Render {
        return getRender()
    }

}