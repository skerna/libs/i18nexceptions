package io.skerna.common.i18nexceptions

interface Renderable {
    fun render(): RenderResult?
}