package io.skerna.commons.i18nexceptions

import io.skerna.common.i18nexceptions.ExceptionRender
import io.skerna.common.i18nexceptions.Render
import io.skerna.common.i18nexceptions.StandardRuntimeException

class ExceptionCode : StandardRuntimeException {
    constructor(errorCode: String) : super(errorCode)


    override fun getRender(): Render {
        return ExceptionRender(TestCatalog)
    }

}