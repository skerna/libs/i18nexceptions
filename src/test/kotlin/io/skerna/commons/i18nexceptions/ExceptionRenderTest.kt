package io.skerna.commons.i18nexceptions

import io.skerna.common.i18nexceptions.Render
import org.junit.jupiter.api.Test

class ExceptionRenderTest {

    @Test
    fun render() {
        val catlog = TestCatalog

        val render = Render.create(catlog)

        val renderResult = render.render(ExceptionCode("CODIGO_ITEM_NOTFOUND"))
        println(renderResult)


        println(ExceptionCode("CODIGO_ITEM_NOTFOUND"))
        println(ExceptionCode("CODIGO_ITEM_NOTFOUND").render().encodeAsJson())
    }
}