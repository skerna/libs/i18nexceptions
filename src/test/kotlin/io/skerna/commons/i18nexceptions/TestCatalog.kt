package io.skerna.commons.i18nexceptions

import io.skerna.common.i18nexceptions.ResourceMessageCatalog

object TestCatalog : ResourceMessageCatalog("") {
    override val bundleName: String
        get() = "scope_a.messages"
}