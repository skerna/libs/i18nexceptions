package io.skerna.commons.i18nexceptions

import io.skerna.common.i18nexceptions.ExceptionRender
import io.skerna.common.i18nexceptions.I18NException
import io.skerna.common.i18nexceptions.I18NInternalException
import io.skerna.common.i18nexceptions.MessageCatalog
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.util.*

class ResourceMessageCatalogTest {

    private var message: MessageCatalog = TestCatalog

    @Test
    @Throws(ExceptionCode::class)
    fun testRuntimeException() {
        try {
            throw ExceptionCode("CODIGO_ITEM_NOTFOUND")
                    .appendMessage("Algo de context en tu idioma ...")


        } catch (ex: Exception) {
            ExceptionRender(message).render(ex as I18NException)
            Assertions.assertEquals(ex.getErrorCode(), "CODIGO_ITEM_NOTFOUND")
        }

    }

    @Test
    @Throws(ExceptionCode::class)
    fun testRuntimeUnknowCode() {
        try {
            val a = ExceptionUnknowCode()
            ExceptionRender(message!!).render(a)
        } catch (ex: Exception) {
            Assertions.assertEquals(ex.javaClass, I18NInternalException::class.java)
        }

    }


    @Test
    fun getLocalizedMessage() {
        Assertions.assertEquals("Ingles Item not found", message!!.getLocalizedMessage("CODIGO_TEST", Locale.ENGLISH))
        Assertions.assertEquals("No encontrado", message!!.getLocalizedMessage("CODIGO_TEST", Locale("es", "EC")))
    }

    @Test
    fun hasCode() {
        val result = message!!.hasCode("CODIGO_ITEM_NOTFOUND")
        Assertions.assertEquals(result, true)
    }

}