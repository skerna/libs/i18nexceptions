package io.skerna.commons.i18nexceptions

import io.skerna.common.i18nexceptions.ExceptionRender
import io.skerna.common.i18nexceptions.Render
import io.skerna.common.i18nexceptions.ResourceMessageCatalog
import io.skerna.common.i18nexceptions.StandardException

class ExceptionUnknowCode internal constructor()// RESOURCE KEY ITEM
    : StandardException("NO_REGISTRADO_BUNDLE") {

    override fun getRender(): Render {
        return ExceptionRender(ResourceMessageCatalog("scope_a.messages"))
    }
}